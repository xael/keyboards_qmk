#include QMK_KEYBOARD_H
#include "sendstring_bepo.h"
#include "quantum/keymap_extras/keymap_bepo.h"

#ifdef CONSOLE_ENABLE
#include "print.h"
#endif


// #ifdef SSD1306OLED
//   #include "ssd1306.h"
// #endif

#define NUM_LAYER_TIMEOUT 60000  //configure your timeout in milliseconds


enum keymap_layer {
  KL_BASE,
  KL_MACOS,
  KL_QWERTY,
  KL_QWERTY_SHIFTED,
  KL_NUMBER_AND_FCT,
  KL_CURSOR_AND_NUMBERS,
  KL_MOUSE_AND_FCT,
  KL_OSL,
};



#ifdef COMBO_ENABLE
enum combos {
  KDF_OP,
  KJK_CP,
  KSD_OA,
  KKL_CA,
  KSF_OB,
  KJL_CB,
  KHJ_CEDIL,
};


const uint16_t PROGMEM kdf_combo[] = {KC_D, KC_F, COMBO_END};
const uint16_t PROGMEM kjk_combo[] = {KC_J, KC_K, COMBO_END};

const uint16_t PROGMEM ksd_combo[] = {KC_S, KC_D, COMBO_END};
const uint16_t PROGMEM kkl_combo[] = {KC_K, KC_L, COMBO_END};


const uint16_t PROGMEM ksf_combo[] = {KC_S, KC_F, COMBO_END};
const uint16_t PROGMEM kjl_combo[] = {KC_J, KC_L, COMBO_END};

const uint16_t PROGMEM khj_combo[] = {KC_H, KC_J, COMBO_END};


combo_t key_combos[COMBO_COUNT] = {
  [KDF_OP] = COMBO(kdf_combo, KC_4),  // (
  [KJK_CP] = COMBO(kjk_combo, KC_5),  // )
  [KSD_OA] = COMBO(ksd_combo, ALGR(KC_X)), // {
  [KKL_CA] = COMBO(kkl_combo, ALGR(KC_C)), // }
  [KSF_OB] = COMBO(ksf_combo, ALGR(KC_4)), // [
  [KJL_CB] = COMBO(kjl_combo, ALGR(KC_5)), // ]
  [KHJ_CEDIL] = COMBO(khj_combo, KC_NUHS), // ç
};
#endif

// https://docs.qmk.fm/#/feature_tap_dance?id=example-4-39quad-function-tap-dance39
typedef enum {
  TD_NONE,
  TD_UNKNOWN,
  TD_SINGLE_TAP,
  TD_SINGLE_HOLD,
  TD_DOUBLE_TAP,
  TD_DOUBLE_HOLD,
  TD_DOUBLE_SINGLE_TAP, // Send two single taps
  TD_TRIPLE_TAP,
  TD_TRIPLE_HOLD
} td_state_t;

typedef struct {
  bool is_press_action;
  td_state_t state;
} td_tap_t;


// Tap Dance declarations
enum {
  TD_WBAK_WFWD,
  TD_C_CCEDIL,
  TD_ESC_ESCX,
  // TD_LT2_SPACE_UNDERSCORE,
  TD_TAB_SHIFT,
};


#ifdef OLED_ENABLE
// https://javl.github.io/image2cpp/
// Invert image color
// plain bytes
// Vertical 1 bit by Pixel


void clear_screen(void) {
  oled_clear();
}

#include "logo_base_layer.c"
#include "logo_cursor_layer.c"
#include "logo_function_layer.c"
#include "logo_gaming_layer.c"
#include "logo_mouse_layer.c"
#include "logo_number_layer.c"
// #include "logo_screen_layer.c"
// #include "logo_text_layer.c"
// #include "logo_zoom_layer.c"
// #include "logo_poulpy.c"



void display_text(const char *data) {
  clear_screen();
  oled_set_cursor(7, 0);
  oled_write_ln("Xael", false);
  oled_set_cursor(7, 1);
  oled_write_ln(data, false); 
}



bool oled_task_user(void) {
   // oled_set_brightness(0);
   switch (get_highest_layer(layer_state|default_layer_state)) {
   case KL_BASE:
     clear_screen();
     break;
   case KL_MACOS:
     clear_screen();
     oled_set_cursor(9, 0);
     oled_write("MacOS", false);
     break;
   case KL_NUMBER_AND_FCT:
     render_logo_number_layer();
     // display_text("Numbers");
     break;
   case KL_CURSOR_AND_NUMBERS:
     render_logo_cursor_layer();
     // display_text("Cursor & num");
     break;
   case KL_MOUSE_AND_FCT:
     render_logo_mouse_layer();
     // display_text("Mouse & FKeys");
     break;
   case KL_OSL:
     render_logo_function_layer();
     // display_text("Functions OSL");
     break;
   case KL_QWERTY:
     render_logo_gaming_layer();
     // display_text("Functions OSL");
     break;
   case KL_QWERTY_SHIFTED:
     render_logo_gaming_layer();
     oled_set_cursor(9, 0);
     oled_write("S", false);
     // display_text("Functions OSL");
     break;
   default:
     // render_logo_base_layer();
     display_text("Error layer");
     break;
   }
#ifdef WPM_ENABLE
  int current_wpm;
  oled_set_cursor(0, 1);
  current_wpm = get_current_wpm();
  char wpm_counter[4];
  wpm_counter[3] = '\0';
  wpm_counter[2] = '0' + current_wpm % 10;
  wpm_counter[1] = '0' + (current_wpm / 10) % 10;
  wpm_counter[0] = '0' + (current_wpm / 100) % 10;
  oled_write(wpm_counter, false);   
#endif 
  return 0;
}


//bool is_num_lock_lit = false;
//bool is_caps_lock_lit = false;
//bool led_update_kb(led_t led_state) {
//    bool res = led_update_user(led_state);
//    if (res) {
//        is_num_lock_lit = led_state.num_lock;
//        is_caps_lock_lit = led_state.caps_lock;
//    }
//    return res;
//}

void post_process_record_user(uint16_t keycode, keyrecord_t *record) {
#ifdef CONSOLE_ENABLE
  // see https://precondition.github.io/qmk-heatmap
  if (record->event.pressed) {
    uprintf("0x%04X,%u,%u,%u,%b,0x%02X,0x%02X,%u\n",
            keycode,
            record->event.key.row,
            record->event.key.col,
            get_highest_layer(layer_state),
            record->event.pressed,
            get_mods(),
            get_oneshot_mods(),
            record->tap.count
            );
  }
#endif
  switch (keycode) {
  case KC_ASON:
    if (!record->event.pressed) {
      display_text("AutoShift ON");
    }
    break;
  case KC_ASOFF:
    if (!record->event.pressed) {
      display_text("AutoShift OFF");
    }
    break;
  case PB_32:
    if (record->event.pressed) {
      tap_code(KC_NUMLOCK);
      wait_ms(20);
      tap_code(KC_NUMLOCK);
    }
    break;
  case PB_31:
    if (record->event.pressed) {
      tap_code(KC_CAPSLOCK);
      wait_ms(250);
      tap_code(KC_CAPSLOCK);
    }
    break;
  case PB_30:
    if (record->event.pressed) {
      tap_code(KC_CAPSLOCK);
      wait_ms(20);
      tap_code(KC_NUMLOCK);
      wait_ms(250);
      tap_code(KC_NUMLOCK);
      wait_ms(20);
      tap_code(KC_CAPSLOCK);
    }
    break;
 }
}
#endif


layer_state_t default_layer_state_set_user(layer_state_t state) {
  switch (get_highest_layer(state)) {
  case KL_QWERTY:
    autoshift_disable();
    break;
  case KL_QWERTY_SHIFTED:
    autoshift_disable();
    break;
  default:
    autoshift_enable();
    break;
  }
  return state;
}


td_state_t cur_dance(qk_tap_dance_state_t *state) {
  if (state->count == 1) {
    if (state->interrupted || !state->pressed) return TD_SINGLE_TAP;
    // Key has not been interrupted, but the key is still held. Means you want to send a 'HOLD'.
    else return TD_SINGLE_HOLD;
  } else if (state->count == 2) {
    // TD_DOUBLE_SINGLE_TAP is to distinguish between typing "pepper", and actually wanting a double tap
    // action when hitting 'pp'. Suggested use case for this return value is when you want to send two
    // keystrokes of the key, and not the 'double tap' action/macro.
    if (state->interrupted) return TD_DOUBLE_SINGLE_TAP;
    else if (state->pressed) return TD_DOUBLE_HOLD;
    else return TD_DOUBLE_TAP;
  }

  // Assumes no one is trying to type the same letter three times (at least not quickly).
  // If your tap dance key is 'KC_W', and you want to type "www." quickly - then you will need to add
  // an exception here to return a 'TD_TRIPLE_SINGLE_TAP', and define that enum just like 'TD_DOUBLE_SINGLE_TAP'
  if (state->count == 3) {
    if (state->interrupted || !state->pressed) return TD_TRIPLE_TAP;
    else return TD_TRIPLE_HOLD;
  } else return TD_UNKNOWN;
}


// Create an instance of 'td_tap_t' for the 'tab' tap dance.
static td_tap_t tabtap_state = {
  .is_press_action = true,
  .state = TD_NONE
};

void tab_shift_finished(qk_tap_dance_state_t *state, void *user_data) {
  tabtap_state.state = cur_dance(state);
  switch (tabtap_state.state) {
  case TD_SINGLE_TAP: register_code(KC_TAB); break;
  case TD_SINGLE_HOLD: register_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: register_code(KC_LSFT); register_code(KC_TAB); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}

void tab_shift_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (tabtap_state.state) {
  case TD_SINGLE_TAP: unregister_code(KC_TAB); break;
  case TD_SINGLE_HOLD: unregister_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: unregister_code(KC_TAB); unregister_code(KC_LSFT); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  tabtap_state.state = TD_NONE;
}

// Create an instance of 'td_tap_t' for the 'esc' tap dance.
static td_tap_t esctap_state = {
  .is_press_action = true,
  .state = TD_NONE
};


void esc_lgui_finished(qk_tap_dance_state_t *state, void *user_data) {
  esctap_state.state = cur_dance(state);
  switch (esctap_state.state) {
  case TD_SINGLE_TAP: register_code(KC_ESC); break;
  case TD_SINGLE_HOLD: register_code(KC_LGUI); break;
  case TD_DOUBLE_TAP: register_code(KC_LALT); register_code(BP_X); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}


void esc_lgui_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (esctap_state.state) {
  case TD_SINGLE_TAP: unregister_code(KC_ESC); break;
  case TD_SINGLE_HOLD: unregister_code(KC_LGUI); break;
  case TD_DOUBLE_TAP: unregister_code(BP_X); unregister_code(KC_LALT); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  esctap_state.state = TD_NONE;
}

// Create an instance of 'td_tap_t' for the 'c' tap dance.
static td_tap_t ctap_state = {
  .is_press_action = true,
  .state = TD_NONE
};

void c_ccedil_finished(qk_tap_dance_state_t *state, void *user_data) {
  ctap_state.state = cur_dance(state);
  switch (ctap_state.state) {
  case TD_SINGLE_TAP: register_code(BP_C); break;
  case TD_SINGLE_HOLD: register_code(KC_LSFT); register_code(BP_C); break;
  case TD_DOUBLE_TAP: register_code(KC_NUHS); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}

void c_ccedil_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (ctap_state.state) {
  case TD_SINGLE_TAP: unregister_code(BP_C); break;
  case TD_SINGLE_HOLD: unregister_code(BP_C); unregister_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: unregister_code(KC_NUHS); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  ctap_state.state = TD_NONE;
}


// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
  [TD_WBAK_WFWD] = ACTION_TAP_DANCE_DOUBLE(KC_WBAK, KC_WFWD),
  [TD_C_CCEDIL] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, c_ccedil_finished, c_ccedil_reset),
  [TD_ESC_ESCX] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, esc_lgui_finished, esc_lgui_reset),
  //[TD_LT2_SPACE_UNDERSCORE] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, space_finished, space_reset),
  [TD_TAB_SHIFT] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, tab_shift_finished, tab_shift_reset)
};




const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  //      /* BÉPO
  //      * +-----------------------------------------+                             +-----------------------------------------+
  //      * | ESC  |   b  |   é  |   p  |   o  |   è  |                             |   ^  |   v  |   d  |   l  |   j  |   =  |
  //      * |------+------+------+------+------+------|                             |------+------+------+------+------+------|
  //      * | TAB  |   b  |   u  |   i  |   e  |   ,  |                             |   c  |   t  |   s  |   r  |   n  |   m  |
  //      * |------+------+------+------+------+------|                             |------+------+------+------+------+------|
  //      * | SHFT |   a  |   y  |   x  |   .  |   k  |                             |   '  |   q  |   g  |   h  |   f  |   w  |
  //      * +------+------+------+------+-------------+                             +-------------+------+------+------+------+


  //     /* Base (qwerty)
  //      * +-----------------------------------------+                             +-----------------------------------------+
  //      * | ESC  |   q  |   w  |   e  |   r  |   t  |                             |   y  |   u  |   i  |   o  |   p  |      |
  //      * |------+------+------+------+------+------|                             |------+------+------+------+------+------|
  //      * | TAB  |   a  |   s  |   d  |   f  |   g  |                             |   h  |   j  |   k  |   l  |   ;  |      |
  //      * |------+------+------+------+------+------|                             |------+------+------+------+------+------|
  //      * | SHFT |   z  |   x  |   c  |   v  |   b  |                             |   n  |   m  |   ,  |   .  |   /  |      |
  //      * +------+------+------+------+-------------+                             +-------------+------+------+------+------+
  //      *               |  [   |   ]  |                                                         |      |      |
  //      *               +-------------+-------------+                             +-------------+-------------+
  //      *                             |      |      |                             |      |      |
  //      *                             |------+------|                             |------+------|
  //      *                             |      |      |                             |      |      |
  //      *                             +-------------+                             +-------------+
  //      *                                           +-------------+ +-------------+
  //      *                                           |      |      | |      |      |
  //      *                                           |------+------| |------+------|
  //      *                                           |      |      | |      |      |
  //      *                                           +-------------+ +-------------+
  //      */
  
  // https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes_basic.md
  // https://github.com/qmk/qmk_firmware/blob/master/docs/keycodes.md
  // https://github.com/qmk/qmk_firmware/blob/master/docs/feature_tap_dance.md
  // https://jayliu50.github.io/qmk-cheatsheet/

  // MT(MOD_RSFT, KC_SPC) -> espace raté trop souvent
  // TD(TD_LT2_SPACE_UNDERSCORE)  -> pas super fiable
  [KL_BASE] = LAYOUT( // Base
               // _______,  _______,  _______,  _______,  _______,  _______,         _______,  _______,  _______,  _______,  _______,  _______,
               BP_DLR,   BP_B,     BP_EACU,  BP_P,     BP_O,     BP_EGRV,                 BP_DCIR,  BP_V,     BP_D,     BP_L,     BP_J,     BP_Z,
               DM_PLY1,  BP_A,     BP_U,     BP_I,     BP_E,     BP_COMM,                 TD(TD_C_CCEDIL),     BP_T,     BP_S,     BP_R,     BP_N,     BP_M,
               DM_PLY2,  BP_AGRV,  BP_Y,     BP_X,     BP_DOT,   BP_K,                    BP_QUOT,  BP_Q,     BP_G,     BP_H,     BP_F,     BP_W,
                                   KC_BTN1,    KC_LEAD,                                           TG(KL_MOUSE_AND_FCT),    TG(KL_NUMBER_AND_FCT),
                             MT(MOD_LCTL, KC_SPC),  MT(MOD_RALT, KC_ENT),                 TD(TD_TAB_SHIFT),  LT(KL_CURSOR_AND_NUMBERS, KC_SPACE),
                                                    KC_LALT,  TD(TD_ESC_ESCX),            MT(MOD_LSFT, KC_TAB),   OSL(KL_OSL),
                                                       KC_F24, KC_F23,                    C(KC_INS),  S(KC_INS)
                ),
  
//   [KL_MACOS] = LAYOUT(
//                // _______,  _______,  _______,  _______,  _______,  _______,         _______,  _______,  _______,  _______,  _______,  _______,
//                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,             KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
//                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,             KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
//                       KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,             KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,
//                                         KC_TRNS, KC_TRNS,                                                 KC_TRNS, KC_TRNS,
//                                                           KC_TRNS, KC_TRNS,              KC_TRNS, KC_TRNS,
//                                                           KC_TRNS, KC_TRNS,              KC_TRNS, KC_TRNS,
//                                                           KC_TRNS, KC_TRNS,              KC_TRNS, KC_TRNS,
//                       ),
//                 ),
  
  [KL_QWERTY] = LAYOUT(
               BP_DLR,   BP_Q,     BP_W,     BP_E,     BP_R,     BP_T,                    BP_Y,     BP_U,     BP_I,     BP_O,     BP_P,     DF(KL_BASE),
               KC_TAB,   BP_A,     BP_S,     BP_D,     BP_F,     BP_G,                    BP_H,     BP_J,     BP_K,     BP_L,     BP_SCLN,  DF(KL_QWERTY_SHIFTED),
               KC_LSFT,  BP_Z,     BP_X,     BP_C,     BP_V,     BP_B,                    BP_N,     BP_M,     BP_COMM,  BP_DOT,   BP_SLSH,  KC_NO,
                                         KC_NO,    KC_NO,                                           KC_TRNS,  KC_TRNS,  
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS
                       ),
  
  [KL_QWERTY_SHIFTED] = LAYOUT(
               KC_NO,  BP_DLR,   BP_Q,     BP_W,     BP_E,     BP_R,     BP_T,                    BP_Y,     BP_U,     BP_I,     BP_O,     BP_P,     
               KC_NO,  KC_TAB,   BP_A,     BP_S,     BP_D,     BP_F,     BP_G,                    BP_H,     BP_J,     BP_K,     BP_L,     BP_SCLN, 
               KC_NO,  KC_LSFT,  BP_Z,     BP_X,     BP_C,     BP_V,     BP_B,                    BP_N,     BP_M,     BP_COMM,  BP_DOT,   BP_SLSH, 
                                         KC_NO,    KC_NO,                                           KC_TRNS,  KC_TRNS,  
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_SPACE,                KC_TRNS,  KC_TRNS
                       ),
  
  [KL_NUMBER_AND_FCT] = LAYOUT(  // Numbers & FUNCTION                                                                                                                                      
               RESET,    KC_F1,     KC_F2,   KC_F3,    KC_F4,    KC_F5,                   KC_F6,    KC_F7,     KC_F8,   KC_F9,    KC_F10,   KC_F11,
               PB_30,    S(KC_1),   S(KC_2), S(KC_3),  S(KC_4),  S(KC_5),                 S(KC_6),  S(KC_7),   S(KC_8), S(KC_9),  S(KC_0),  KC_PDOT,
               KC_TRNS,  KC_1,      KC_2,    KC_3,     KC_4,     KC_5,                    KC_6,     KC_7,      KC_8,    KC_9,     KC_0,     KC_EQL,
                                    KC_TRNS, KC_TRNS,                                                          TO(KL_MOUSE_AND_FCT),   KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS
                 ),
  [KL_CURSOR_AND_NUMBERS] = LAYOUT(  // Cursor & numbers                                                      
               DM_RSTP,  S(KC_1),   S(KC_2), S(KC_3),  S(KC_4),  S(KC_5),                 KC_INS,   KC_HOME,   KC_UP,   KC_END,   C(KC_PGUP),  C(BP_W),
               DM_REC1,  S(KC_6),   S(KC_7), S(KC_8),  S(KC_9),  S(KC_0),                 KC_BSPC,  KC_LEFT,   KC_DOWN, KC_RGHT,  C(KC_PGDN),  A(KC_9),
               DM_REC2,  KC_7,      KC_8,    KC_9,     KC_0,     KC_MINS,                 KC_DEL,   KC_PGUP,   KC_NO,   KC_PGDN,  KC_NO,    A(BP_PERC),
                                    KC_TRNS, KC_TRNS,                                                          KC_TRNS, KC_TRNS,
                                                       OSM(MOD_LCTL),  OSM(MOD_LSFT),     KC_TRNS,  KC_TRNS,
                                                       OSM(MOD_LALT),  OSM(MOD_LGUI),     KC_TRNS,  KC_TRNS,
                                                       OSM(MOD_RALT),  KC_MAIL,           KC_TRNS,  KC_TRNS
               ),                                                                                                                           
  [KL_MOUSE_AND_FCT] = LAYOUT(  // Mouse & functions                                                        
               KC_TRNS,  KC_F1,     KC_F2,   KC_F3,    KC_F4,    KC_F5,                   KC_MAIL,         KC_BTN1,   KC_MS_U, KC_BTN2,  KC_WHOM,  C(BP_W),
               PB_31,    KC_F6,     KC_F7,   KC_F8,    KC_F9,    KC_F10,                  KC_MS_WH_LEFT,   KC_MS_L,   KC_MS_D, KC_MS_R,  KC_WH_D,  C(KC_PGUP),
               PB_32,    KC_BTN3,   KC_WH_D, KC_WH_U,  KC_BTN4,  KC_F11,                  KC_MS_WH_RIGHT,  KC_BTN3,   KC_NO,   KC_BTN4,  TD(TD_WBAK_WFWD),    C(KC_PGDN),
                                    KC_BTN1, KC_BTN2,                                                          KC_TRNS, TO(KL_NUMBER_AND_FCT),
                                                       KC_TRNS,  KC_TRNS,                 KC_WH_U,  KC_WH_D,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS,
                                                       KC_TRNS,  KC_TRNS,                 KC_TRNS,  KC_TRNS
                 ),
  [KL_OSL] = LAYOUT(  // OSL                                                      
               KC_MAIL,   KC_1,      KC_2,     KC_3,     KC_4,     KC_5,                    KC_6,     KC_7,      KC_8,    KC_9,     KC_0,     KC_EQL,
               KC_F21,    KC_NO,     KC_NO,    KC_NO,    KC_NO,    PB_30,                   KC_1,  KC_4,      ALGR(KC_X),   ALGR(KC_4),    ALGR(KC_2),  KC_NO,
               KC_F22,    KC_ASON,   KC_ASOFF, DF(KL_BASE),    DF(KL_QWERTY),    DF(KL_QWERTY_SHIFTED),           KC_1,     KC_5,      ALGR(KC_C),   ALGR(KC_5),    ALGR(KC_3),  S(KC_EQL),
                                    PB_32,  PB_31,                                                        A(BP_A),  A(BP_V),
                                                       G(KC_1),  G(KC_2),                 G(KC_8),  G(KC_7),
                                                       G(KC_6),  G(KC_3),                 G(KC_9),  KC_NO,
                                                       G(KC_5),  G(KC_4),                 G(KC_0),  G(KC_MNXT)
                      )
};



LEADER_EXTERNS();

void matrix_scan_user(void) {


  if (get_highest_layer(layer_state) == KL_MOUSE_AND_FCT) {
    if (last_input_activity_elapsed() > NUM_LAYER_TIMEOUT) {
      layer_off(KL_MOUSE_AND_FCT);
    }
  }

  
  LEADER_DICTIONARY() {
    leading = false;
    leader_end();

    // quote -> cut sound and video in Zoom
    SEQ_ONE_KEY(BP_QUOT) {
      register_code(KC_LALT);
      register_code(BP_A);
      unregister_code(BP_A);
      unregister_code(KC_LALT);
      register_code(KC_LALT);
      register_code(BP_V);
      unregister_code(BP_V);
      unregister_code(KC_LALT);
    }

    // Q -> save in emacs
    SEQ_ONE_KEY(BP_G) {
      register_code(KC_LCTL);
      register_code(BP_X);
      unregister_code(BP_X);
      register_code(BP_S);
      unregister_code(BP_S);
      unregister_code(KC_LCTL);
    }
    SEQ_ONE_KEY(BP_Q) { 
      SEND_STRING(SS_TAP(X_1) SS_TAP(X_1) SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_T) {
      SEND_STRING("()" SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_S) {
      SEND_STRING("{}" SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_R) {
      SEND_STRING("[]" SS_TAP(X_LEFT));
    }

    /* // P I */
    /* SEQ_TWO_KEYS(BP_P, BP_I) { */
    /*   SEND_STRING("import ipdb; ipdb.set_trace()"); */
    /* } */

    /* // P W */
    /* SEQ_TWO_KEYS(BP_P, BP_W) { */
    /*   SEND_STRING("import wdb; wdb.set_trace()"); */
    /* } */

    // E X
    SEQ_TWO_KEYS(BP_E, BP_X) {
      SEND_STRING("norman@xael.org");
    }
    // E H
    SEQ_TWO_KEYS(BP_E, BP_H) {
      SEND_STRING("alexandre.norman@hespul.org");
    }
  }
}


