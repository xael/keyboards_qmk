static void render_logo_gaming_layer(void) {

  static const char PROGMEM raw_logo1[] = {
    0, 0, 0, 192, 224, 240, 248, 120, 60, 60, 30, 30, 14, 14, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 15, 14, 14, 30, 30, 60, 124, 120, 248, 240, 224, 128, 0, 0, 0
  };
  static const char PROGMEM raw_logo2[] = {
    192, 252, 255, 255, 15, 3, 0, 0, 0, 0, 192, 192, 192, 192, 252, 252, 252, 252, 192, 192, 192, 192, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 60, 60, 60, 0, 0, 0, 0, 0, 0, 1, 3, 31, 255, 255, 252, 192
  };
  static const char PROGMEM raw_logo3[] = {
    3, 63, 255, 255, 240, 192, 128, 0, 0, 0, 3, 3, 3, 3, 63, 63, 63, 63, 3, 3, 3, 3, 0, 0, 0, 0, 0, 0, 0, 0, 0, 60, 60, 60, 60, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 128, 192, 248, 255, 255, 63, 3
  };
  static const char PROGMEM raw_logo4[] = {
    0, 0, 0, 3, 7, 15, 15, 30, 62, 60, 120, 120, 120, 112, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 240, 112, 120, 120, 120, 60, 62, 30, 15, 15, 7, 1, 0, 0, 0
  };

  clear_screen();
  
  oled_set_cursor(0, 0);
  oled_write_raw_P(raw_logo1, sizeof(raw_logo1));
  oled_set_cursor(0, 1);
  oled_write_raw_P(raw_logo2, sizeof(raw_logo2));
  oled_set_cursor(0, 2);
  oled_write_raw_P(raw_logo3, sizeof(raw_logo3));
  oled_set_cursor(0, 3);
  oled_write_raw_P(raw_logo4, sizeof(raw_logo4));

}
