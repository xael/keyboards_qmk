static void render_logo_mouse_layer(void) {

  static const char PROGMEM raw_logo1[] = {
    0, 224, 240, 120, 28, 14, 14, 6, 6, 7, 7, 255, 255, 7, 7, 6, 6, 14, 28, 60, 120, 240, 224, 0
  };
  static const char PROGMEM raw_logo2[] = {
    255, 255, 255, 56, 56, 56, 56, 56, 56, 56, 56, 63, 63, 56, 56, 56, 56, 56, 56, 56, 56, 255, 255, 254
  };
  static const char PROGMEM raw_logo3[] = {
    255, 255, 255, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 255, 255, 255
  };
  static const char PROGMEM raw_logo4[] = {
    0, 7, 15, 30, 56, 112, 112, 96, 224, 224, 224, 224, 224, 224, 224, 224, 96, 112, 112, 56, 30, 15, 7, 0
  };

  render_logo_base_layer();
  
  oled_set_cursor(6, 0);
  oled_write_raw_P(raw_logo1, sizeof(raw_logo1));
  oled_set_cursor(6, 1);
  oled_write_raw_P(raw_logo2, sizeof(raw_logo2));
  oled_set_cursor(6, 2);
  oled_write_raw_P(raw_logo3, sizeof(raw_logo3));
  oled_set_cursor(6, 3);
  oled_write_raw_P(raw_logo4, sizeof(raw_logo4));
  
}
