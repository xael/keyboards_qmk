static void render_logo_function_layer(void) {
  static const char PROGMEM raw_logo1[] = {
    // 'function_layer2', 39x32px
    0, 0, 0, 0, 248, 252, 254, 30, 15, 15, 15, 15, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0    
  };
  static const char PROGMEM raw_logo2[] = {
    240, 240, 240, 240, 255, 255, 255, 240, 240, 240, 96, 0, 0, 0, 192, 248, 254, 254, 31, 6, 0, 128, 128, 128, 0, 0, 0, 0, 128, 128, 128, 0, 4, 30, 127, 254, 248, 224, 0
  };
  static const char PROGMEM raw_logo3[] = {
    0, 0, 0, 1, 255, 255, 255, 0, 0, 0, 0, 0, 0, 60, 255, 255, 255, 0, 0, 0, 0, 195, 231, 255, 255, 126, 126, 255, 255, 231, 195, 0, 0, 0, 0, 255, 255, 255, 255
  };
  static const char PROGMEM raw_logo4[] = {
    60, 60, 60, 31, 31, 15, 7, 0, 0, 0, 0, 0, 0, 0, 3, 31, 127, 255, 248, 96, 0, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 0, 96, 240, 254, 127, 63, 15, 0
  };

  render_logo_base_layer();
  
  oled_set_cursor(8, 0);
  oled_write_raw_P(raw_logo1, sizeof(raw_logo1));
  oled_set_cursor(8, 1);
  oled_write_raw_P(raw_logo2, sizeof(raw_logo2));
  oled_set_cursor(8, 2);
  oled_write_raw_P(raw_logo3, sizeof(raw_logo3));
  oled_set_cursor(8, 3);
  oled_write_raw_P(raw_logo4, sizeof(raw_logo4));

}
