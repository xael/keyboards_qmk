# MCU name
# MCU = atmega32u4
# blackpill
# MCU = STM32F401S
# STM32_BOOTLOADER_ADDRESS = 0x1FFF0000

# Bootloader selection
# BOOTLOADER = caterina
# BOOTLOADER = atmel-dfu

# left part
# BOOTLOADER = qmk-dfu
# right part
# BOOTLOADER = caterina
# black pill
# BOOTLOADER = stm32-dfu

# Build Options
#   change yes to no to disable
#

AUDIO_ENABLE = no
AUTO_SHIFT_ENABLE = yes
AUTO_SHIFT_MODIFIERS = yes
BACKLIGHT_ENABLE = no
BLUETOOTH_ENABLE = no
BOOTMAGIC_ENABLE = no       # Enable Bootmagic Lite
COMBO_ENABLE = no
COMMAND_ENABLE = no        # Commands for debug and configuration
CONSOLE_ENABLE = no         # Console for debug
DYNAMIC_MACRO_ENABLE = yes
EXTRAKEY_ENABLE = yes       # Audio control and System control
FAUXCLICKY_ENABLE = no
FORCE_NKRO = no
GRAVE_ESC_ENABLE = no 
HD44780_ENABLE = no
LEADER_ENABLE = yes
LTO_ENABLE = yes
MAGIC_ENABLE = no
MIDI_ENABLE	= no
MOUSEKEY_ENABLE = yes
MUSIC_ENABLE = no
NKRO_ENABLE = no
OLED_DRIVER = SSD1306
OLED_ENABLE = yes
RGBLIGHT_ENABLE = no
SLEEP_LED_ENABLE = no
SPACE_CADET_ENABLE = no
SPLIT_KEYBOARD = yes
TAP_DANCE_ENABLE = yes
# UNICODE_ENABLE = no
# WPM_ENABLE = yes

NO_USB_STARTUP_CHECK = yes
KEYBOARD_SHARED_EP = no
MOUSE_SHARED_EP = no

EXTRAFLAGS += -flto

