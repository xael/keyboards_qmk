static void render_logo_base_layer(void) {
  static const char PROGMEM raw_logo1[] = {
    0, 0, 192, 192, 192, 192, 192, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 252, 248, 240, 224, 224, 224, 224, 224, 224, 240, 248, 252, 0, 0, 0, 0, 0, 0
  };
  
  static const char PROGMEM raw_logo2[] = {
    0, 0, 1, 1, 1, 7, 255, 255, 254, 0, 128, 192, 224, 224, 240, 240, 240, 240, 240, 240, 192, 143, 31, 63, 124, 124, 127, 127, 127, 124, 60, 31, 15, 0, 0, 0, 0, 0, 0
  };
  
  static const char PROGMEM raw_logo3[] = {
    0, 0, 0, 0, 0, 0, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 255, 127, 127, 62, 30, 254, 252, 252, 252, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  };
  
  static const char PROGMEM raw_logo4[] = {
    0, 0, 0, 0, 0, 0, 31, 63, 127, 127, 127, 127, 127, 127, 127, 127, 127, 127, 123, 121, 112, 0, 0, 0, 0, 63, 127, 127, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
  };

  clear_screen();
  
  oled_set_cursor(0, 0);
  oled_write_raw_P(raw_logo1, sizeof(raw_logo1));
  oled_set_cursor(0, 1);
  oled_write_raw_P(raw_logo2, sizeof(raw_logo2));
  oled_set_cursor(0, 2);
  oled_write_raw_P(raw_logo3, sizeof(raw_logo3));
  oled_set_cursor(0, 3);
  oled_write_raw_P(raw_logo4, sizeof(raw_logo4));
}
