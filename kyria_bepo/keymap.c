/* Copyright 2019 Thomas Baart <thomas@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H
#include "sendstring_bepo.h"
#include "quantum/keymap_extras/keymap_bepo.h"

uint16_t copy_paste_timer;

#define NUM_LAYER_TIMEOUT 60000  //configure your timeout in milliseconds


enum keymap_layer {
  KL_BASE,
  KL_NUMBER_AND_FCT,
  KL_CURSOR_AND_NUMBERS,
  KL_MOUSE_AND_FCT,
  KL_OSL,
  KL_EMPTY,
};



#ifdef COMBO_ENABLE
enum combos {
  KDF_OP,
  KJK_CP,
  KSD_OA,
  KKL_CA,
  KSF_OB,
  KJL_CB,
  KHJ_CEDIL,
};


const uint16_t PROGMEM kdf_combo[] = {KC_D, KC_F, COMBO_END};
const uint16_t PROGMEM kjk_combo[] = {KC_J, KC_K, COMBO_END};

const uint16_t PROGMEM ksd_combo[] = {KC_S, KC_D, COMBO_END};
const uint16_t PROGMEM kkl_combo[] = {KC_K, KC_L, COMBO_END};


const uint16_t PROGMEM ksf_combo[] = {KC_S, KC_F, COMBO_END};
const uint16_t PROGMEM kjl_combo[] = {KC_J, KC_L, COMBO_END};

const uint16_t PROGMEM khj_combo[] = {KC_H, KC_J, COMBO_END};


combo_t key_combos[COMBO_COUNT] = {
  [KDF_OP] = COMBO(kdf_combo, KC_4),  // (
  [KJK_CP] = COMBO(kjk_combo, KC_5),  // )
  [KSD_OA] = COMBO(ksd_combo, ALGR(KC_X)), // {
  [KKL_CA] = COMBO(kkl_combo, ALGR(KC_C)), // }
  [KSF_OB] = COMBO(ksf_combo, ALGR(KC_4)), // [
  [KJL_CB] = COMBO(kjl_combo, ALGR(KC_5)), // ]
  [KHJ_CEDIL] = COMBO(khj_combo, KC_NUHS), // ç
};
#endif

// https://docs.qmk.fm/#/feature_tap_dance?id=example-4-39quad-function-tap-dance39
typedef enum {
  TD_NONE,
  TD_UNKNOWN,
  TD_SINGLE_TAP,
  TD_SINGLE_HOLD,
  TD_DOUBLE_TAP,
  TD_DOUBLE_HOLD,
  TD_DOUBLE_SINGLE_TAP, // Send two single taps
  TD_TRIPLE_TAP,
  TD_TRIPLE_HOLD
} td_state_t;

typedef struct {
  bool is_press_action;
  td_state_t state;
} td_tap_t;


// Tap Dance declarations
enum {
  TD_WBAK_WFWD,
  TD_C_CCEDIL,
  TD_ESC_ESCX,
  // TD_LT2_SPACE_UNDERSCORE,
  TD_TAB_SHIFT,
};



void post_process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
  case PB_32:
    if (record->event.pressed) {
      tap_code(KC_NUMLOCK);
      wait_ms(20);
      tap_code(KC_NUMLOCK);
    }
    break;
  case PB_31:
    if (record->event.pressed) {
      tap_code(KC_CAPSLOCK);
      wait_ms(250);
      tap_code(KC_CAPSLOCK);
    }
    break;
  case PB_30:
    if (record->event.pressed) {
      tap_code(KC_CAPSLOCK);
      wait_ms(20);
      tap_code(KC_NUMLOCK);
      wait_ms(250);
      tap_code(KC_NUMLOCK);
      wait_ms(20);
      tap_code(KC_CAPSLOCK);
    }
    break;
 }
}



td_state_t cur_dance(qk_tap_dance_state_t *state) {
  if (state->count == 1) {
    if (state->interrupted || !state->pressed) return TD_SINGLE_TAP;
    // Key has not been interrupted, but the key is still held. Means you want to send a 'HOLD'.
    else return TD_SINGLE_HOLD;
  } else if (state->count == 2) {
    // TD_DOUBLE_SINGLE_TAP is to distinguish between typing "pepper", and actually wanting a double tap
    // action when hitting 'pp'. Suggested use case for this return value is when you want to send two
    // keystrokes of the key, and not the 'double tap' action/macro.
    if (state->interrupted) return TD_DOUBLE_SINGLE_TAP;
    else if (state->pressed) return TD_DOUBLE_HOLD;
    else return TD_DOUBLE_TAP;
  }

  // Assumes no one is trying to type the same letter three times (at least not quickly).
  // If your tap dance key is 'KC_W', and you want to type "www." quickly - then you will need to add
  // an exception here to return a 'TD_TRIPLE_SINGLE_TAP', and define that enum just like 'TD_DOUBLE_SINGLE_TAP'
  if (state->count == 3) {
    if (state->interrupted || !state->pressed) return TD_TRIPLE_TAP;
    else return TD_TRIPLE_HOLD;
  } else return TD_UNKNOWN;
}


// Create an instance of 'td_tap_t' for the 'tab' tap dance.
static td_tap_t tabtap_state = {
  .is_press_action = true,
  .state = TD_NONE
};

void tab_shift_finished(qk_tap_dance_state_t *state, void *user_data) {
  tabtap_state.state = cur_dance(state);
  switch (tabtap_state.state) {
  case TD_SINGLE_TAP: register_code(KC_TAB); break;
  case TD_SINGLE_HOLD: register_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: register_code(KC_LSFT); register_code(KC_TAB); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}

void tab_shift_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (tabtap_state.state) {
  case TD_SINGLE_TAP: unregister_code(KC_TAB); break;
  case TD_SINGLE_HOLD: unregister_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: unregister_code(KC_TAB); unregister_code(KC_LSFT); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  tabtap_state.state = TD_NONE;
}

// Create an instance of 'td_tap_t' for the 'esc' tap dance.
static td_tap_t esctap_state = {
  .is_press_action = true,
  .state = TD_NONE
};


void esc_lgui_finished(qk_tap_dance_state_t *state, void *user_data) {
  esctap_state.state = cur_dance(state);
  switch (esctap_state.state) {
  case TD_SINGLE_TAP: register_code(KC_ESC); break;
  case TD_SINGLE_HOLD: register_code(KC_LGUI); break;
  case TD_DOUBLE_TAP: register_code(KC_LALT); register_code(BP_X); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}

void esc_lgui_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (esctap_state.state) {
  case TD_SINGLE_TAP: unregister_code(KC_ESC); break;
  case TD_SINGLE_HOLD: unregister_code(KC_LGUI); break;
  case TD_DOUBLE_TAP: unregister_code(BP_X); unregister_code(KC_LALT); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  esctap_state.state = TD_NONE;
}

// Create an instance of 'td_tap_t' for the 'esc' tap dance.
static td_tap_t ctap_state = {
  .is_press_action = true,
  .state = TD_NONE
};

void c_ccedil_finished(qk_tap_dance_state_t *state, void *user_data) {
  ctap_state.state = cur_dance(state);
  switch (ctap_state.state) {
  case TD_SINGLE_TAP: register_code(BP_C); break;
  case TD_SINGLE_HOLD: register_code(KC_LSFT); register_code(BP_C); break;
  case TD_DOUBLE_TAP: register_code(KC_NUHS); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}

void c_ccedil_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (ctap_state.state) {
  case TD_SINGLE_TAP: unregister_code(BP_C); break;
  case TD_SINGLE_HOLD: unregister_code(BP_C); unregister_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: unregister_code(KC_NUHS); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  ctap_state.state = TD_NONE;
}


// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
  [TD_WBAK_WFWD] = ACTION_TAP_DANCE_DOUBLE(KC_WBAK, KC_WFWD),
  [TD_C_CCEDIL] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, c_ccedil_finished, c_ccedil_reset),
  [TD_ESC_ESCX] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, esc_lgui_finished, esc_lgui_reset),
  //[TD_LT2_SPACE_UNDERSCORE] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, space_finished, space_reset),
  [TD_TAB_SHIFT] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, tab_shift_finished, tab_shift_reset)
};



// enum custom_keycodes {
//     KC_CCCV = SAFE_RANGE
// };

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
 * Base Layer: BÉPO
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * | $      |   B  |   É  |   P  |   O  |   È  |                              |   !  |   V  |   D  |   L  |   J  |   Z    |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * |  SFT   |   A  |   S  |  D   |   F  |   ,; |                              |   C  |   T  |   S  |   R  |   N  |   M    |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * | LCTL   |   À  |   Y  |   X  |   .: |   K  | ESC  | F23  |  | COPY | OSL  |   '? |   Q  |   G  |   H  |   F  |   W    |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Lalt | WIN  | RET  | Lctl | F24  |  | PASTE| TAB  | SPC  |  TG  | TG   |
 *                        |      |      | Ralt | Spce |      |  |      | shift| curs | mouse| numb |
 *                        `----------------------------------'  `----------------------------------'
 */
   [KL_BASE] = LAYOUT(
      BP_DLR,    BP_B,      BP_EACU,       BP_P,     BP_O,      BP_EGRV,                                                      BP_DCIR,     BP_V,     BP_D,    BP_L,     BP_J,     BP_Z,
      KC_BTN2,   BP_A,      BP_U,          BP_I,     BP_E,      BP_COMM,                                               TD(TD_C_CCEDIL),    BP_T,     BP_S,    BP_R,     BP_N,     BP_M,
      KC_BTN1,   BP_AGRV,   BP_Y,          BP_X,     BP_DOT,    BP_K,   TD(TD_ESC_ESCX),  KC_F23,   C(KC_INS), OSL(KL_OSL),      BP_QUOT,     BP_Q,     BP_G,    BP_H,     BP_F,     BP_W,
      KC_LALT,   KC_LGUI,   MT(MOD_LCTL, KC_SPC), MT(MOD_RALT, KC_ENT), KC_F24,              S(KC_INS), TD(TD_TAB_SHIFT), LT(KL_CURSOR_AND_NUMBERS, KC_SPACE), TG(KL_MOUSE_AND_FCT), TG(KL_NUMBER_AND_FCT)
    ),
 

  [KL_NUMBER_AND_FCT] = LAYOUT(  // Numbers & FUNCTION                                                                                                                                      
               KC_TRNS,    KC_F1,     KC_F2,   KC_F3,    KC_F4,    KC_F5,                                                      KC_F6,    KC_F7,     KC_F8,   KC_F9,    KC_F10,   KC_F11,
               KC_TRNS,    S(KC_1),   S(KC_2), S(KC_3),  S(KC_4),  S(KC_5),                                                    S(KC_6),  S(KC_7),   S(KC_8), S(KC_9),  S(KC_0),  KC_PDOT,
               KC_TRNS,    KC_1,      KC_2,    KC_3,     KC_4,     KC_5,       KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,         KC_6,     KC_7,      KC_8,    KC_9,     KC_0,     KC_EQL,
               KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,        KC_TRNS, KC_TRNS, KC_TRNS, TO(KL_MOUSE_AND_FCT), KC_TRNS       
                 ),
  [KL_CURSOR_AND_NUMBERS] = LAYOUT(  // Cursor & numbers                                                      
               KC_TRNS,  S(KC_1),   S(KC_2), S(KC_3),  S(KC_4),  S(KC_5),                                                    KC_INS,   KC_HOME,   KC_UP,   KC_END,   C(KC_PGUP),  C(BP_W),
               KC_TRNS,  S(KC_6),   S(KC_7), S(KC_8),  S(KC_9),  S(KC_0),                                                    KC_BSPC,  KC_LEFT,   KC_DOWN, KC_RGHT,  C(KC_PGDN),  A(KC_9),
               BP_PERC,  KC_7,      KC_8,    KC_9,     KC_0,     KC_MINS,    KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,         KC_DEL,   KC_PGUP,   KC_NO,   KC_PGDN,  KC_NO,    A(BP_PERC),
               KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS,        KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS, KC_TRNS
               ),                                                                                                                           
  [KL_MOUSE_AND_FCT] = LAYOUT(  // Mouse & functions                                                        
               KC_ACL0,  KC_F1,     KC_F2,   KC_F3,    KC_F4,    KC_F5,                                                      KC_MAIL,         KC_BTN1,   KC_MS_U, KC_BTN2,  KC_WHOM,  C(BP_W),
               KC_ACL1,  KC_F6,     KC_F7,   KC_F8,    KC_F9,    KC_F10,                                                     KC_MS_WH_LEFT,   KC_MS_L,   KC_MS_D, KC_MS_R,  KC_WH_D,  C(KC_PGUP),
               KC_ACL2,  KC_F11,    KC_BTN3,  KC_MS_WH_LEFT,   KC_MS_WH_RIGHT, KC_BTN4,       KC_TRNS,  PB_32,   KC_TRNS,  KC_WH_U,         KC_MS_WH_RIGHT,  KC_BTN3,   KC_NO,   KC_BTN4,  TD(TD_WBAK_WFWD),    C(KC_PGDN),
               KC_BTN1,  KC_BTN2,   KC_TRNS, KC_TRNS,  KC_TRNS,        KC_TRNS, KC_WH_D, KC_TRNS, KC_TRNS, TO(KL_NUMBER_AND_FCT)
                 ),
  [KL_OSL] = LAYOUT(  // OSL                                                      
               KC_MAIL,   KC_1,      KC_2,    KC_3,     KC_4,     KC_5,                                                       KC_6,     KC_7,      KC_8,    KC_9,     KC_0,     KC_EQL,
               PB_31,    KC_NO,     KC_NO,   KC_NO,    PB_31,    PB_32,                                                      KC_1,     KC_4,      ALGR(KC_X),   ALGR(KC_4),    ALGR(KC_2),  KC_NO,
               PB_32,    KC_F24,    KC_F23,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,          KC_1,     KC_5,      ALGR(KC_C),   ALGR(KC_5),    ALGR(KC_3),  S(KC_EQL),
               G(KC_1),  G(KC_2),  G(KC_3),  G(KC_4),  G(KC_5),    G(KC_6),  G(KC_7),  G(KC_8),  G(KC_9),  G(KC_0)
                 ),
    
  [KL_EMPTY] = LAYOUT(  // EMPTY coz bug with OSL which does not work without
               KC_MAIL,   KC_1,      KC_2,    KC_3,     KC_4,     KC_5,                                                       KC_6,     KC_7,      KC_8,    KC_9,     KC_0,     KC_EQL,
               KC_F21,    KC_NO,     KC_NO,   KC_NO,    KC_NO,    KC_NO,                                                      KC_1,     KC_4,      ALGR(KC_X),   ALGR(KC_4),    ALGR(KC_2),  KC_NO,
               KC_F22,    KC_F24,    KC_F23,  KC_TRNS,  KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,   KC_TRNS,  KC_TRNS,          KC_1,     KC_5,      ALGR(KC_C),   ALGR(KC_5),    ALGR(KC_3),  S(KC_EQL),
               G(KC_1),  G(KC_2),  G(KC_3),  G(KC_4),  G(KC_5),    G(KC_6),  G(KC_7),  G(KC_8),  G(KC_9),  G(KC_0)
                 )
    
};


// bool process_record_user(uint16_t keycode, keyrecord_t *record) {
//     switch (keycode) {
//         case KC_CCCV:  // One key copy/paste
//             if (record->event.pressed) {
//                 copy_paste_timer = timer_read();
//             } else {
//                 if (timer_elapsed(copy_paste_timer) > TAPPING_TERM) {  // Hold, copy
//                     tap_code16(LCTL(KC_C));
//                 } else { // Tap, paste
//                     tap_code16(LCTL(KC_V));
//                 }
//             }
//             break;
//     }
//     return true;
// }


// bool is_alt_tab_active = false;
// uint16_t alt_tab_timer = 0;



#ifdef OLED_ENABLE
oled_rotation_t oled_init_user(oled_rotation_t rotation) {
	return OLED_ROTATION_180;
}

static void render_kyria_logo(void) {
    static const char PROGMEM kyria_logo[] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf0, 0xf0, 0xc0, 0x80, 
	0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x80, 0x80, 0x80, 0xc0, 0xf0, 0xf0, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x0f, 0xf9, 0xf9, 
	0x07, 0x07, 0x07, 0x06, 0x06, 0x1e, 0x1c, 0xf8, 0xf8, 0xf8, 0xf8, 0xf0, 0xe0, 0xe0, 0xe0, 0xe0, 
	0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xf0, 0xf8, 0xf8, 0xf8, 0xf8, 0xfc, 0x1e, 0x1e, 0x06, 
	0x07, 0x07, 0x07, 0xf9, 0xf9, 0xf9, 0x0f, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x80, 0x80, 0x80, 0xc0, 0xc0, 0xc0, 0xc0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x1f, 
	0xfc, 0xfc, 0xfc, 0xff, 0xff, 0x3f, 0x3f, 0x0f, 0x0f, 0xcf, 0xcf, 0x0f, 0x0f, 0x0f, 0x3f, 0x3f, 
	0xff, 0xff, 0xff, 0xff, 0x3f, 0x3f, 0x0f, 0x0f, 0xcf, 0xcf, 0x0f, 0x0f, 0x0f, 0x3f, 0x3f, 0xff, 
	0xff, 0xfc, 0xfc, 0x0f, 0x0f, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0xfc, 0xff, 0xff, 
	0xff, 0x9f, 0x8f, 0x03, 0x03, 0x0f, 0x1f, 0xff, 0xfe, 0xfc, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x7f, 0xff, 0xff, 0xff, 0xff, 0xe0, 0xe0, 0x80, 0x80, 0x8f, 0x8f, 0x80, 0x80, 0x80, 0xe0, 0x60, 
	0x7f, 0x7f, 0x7f, 0x7f, 0xe0, 0xe0, 0x80, 0x80, 0x8f, 0x8f, 0x80, 0x80, 0x80, 0xe0, 0xe0, 0xff, 
	0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03, 0x07, 
	0x07, 0x07, 0x87, 0xc0, 0xe0, 0xfe, 0xff, 0x7f, 0x1f, 0x0f, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x03, 0x87, 0xc7, 0xc7, 0xef, 0xff, 0xff, 0x3f, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xfe, 
	0xfe, 0xfe, 0xfe, 0xfe, 0xff, 0xff, 0xff, 0xff, 0x7f, 0x3f, 0x1f, 0x1f, 0x1f, 0x0f, 0x07, 0x07, 
	0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0xfe, 0xff, 0xff, 0xff, 0x0f, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xe0, 0xf0, 0xf8, 
	0xfc, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf9, 0xe0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x01, 0x03, 0x1f, 0x3f, 0x7f, 0xfe, 0xfc, 0xf8, 0xf0, 0xf0, 0xf8, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xfc, 0xf8, 0xf0, 0xe0, 0xe0, 
	0xc0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 
	0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 
	0x0f, 0x0f, 0x0f, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

// 
// 128,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
//  15, 31, 62,124,240,224,192,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
// 240,192,  0,  0,  1,  7, 31,255,252,240,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
// 255,255,254,  0,  0,  0,  0,  0,255,255,255,  0,  0,  0,  0,255,255,  0,  0,192,192, 48, 48,  0,  0,240,240,  0,  0,  0,  0,  0,  0,240,240,  0,  0,240,240,192,192, 48, 48, 48, 48,192,192,  0,  0, 48, 48,243,243,  0,  0,  0,  0,  0,  0, 48, 48, 48, 48, 48, 48,192,192,  0,  0,  0,  0,  0,
// 255,255,127,  0,  0,  0,  0,  0,255,255,255,  0,  0,  0,  0, 63, 63,  3,  3, 12, 12, 48, 48,  0,  0,  0,  0, 51, 51, 51, 51, 51, 51, 15, 15,  0,  0, 63, 63,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 48, 48, 63, 63, 48, 48,  0,  0, 12, 12, 51, 51, 51, 51, 51, 51, 63, 63,  0,  0,  0,  0,  0,
//  15,  3,  0,  0,128,224,248,255, 63, 15,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
// 240,248,124, 62, 15,  7,  3,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,
//   1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0
    };
    oled_write_raw_P(kyria_logo, sizeof(kyria_logo));
}

// static void render_qmk_logo(void) {
//   static const char PROGMEM qmk_logo[] = {
//     0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
//     0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
//     0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,0};
// 
//   oled_write_P(qmk_logo, false);
// }

static void render_status(void) {
    // QMK Logo and version information
    // render_qmk_logo();
    oled_write_P(PSTR("Xael\n"), false);
    oled_write_P(PSTR("Kyria rev2.1\n\n"), false);

    // Host Keyboard Layer Status
    oled_write_P(PSTR("Layer: "), false);
    switch (get_highest_layer(layer_state)) {
        case KL_BASE:
            oled_write_P(PSTR("Bepo\n"), false);
            break;
        case KL_NUMBER_AND_FCT:
            oled_write_P(PSTR("123 & F1\n"), false);
            break;
        case KL_CURSOR_AND_NUMBERS:
            oled_write_P(PSTR("Curseur\n"), false);
            break;
        case KL_MOUSE_AND_FCT:
            oled_write_P(PSTR("Mouse\n"), false);
            break;
        case KL_OSL:
            oled_write_P(PSTR("OSL\n"), false);
            break;
        // case QWERTY:
        //     oled_write_P(PSTR("Default\n"), false);
        //     break;
        // case LOWER:
        //     oled_write_P(PSTR("Lower\n"), false);
        //     break;
        // case RAISE:
        //     oled_write_P(PSTR("Raise\n"), false);
        //     break;
        // case NAV:
        //     oled_write_P(PSTR("Navigation\n"), false);
        //     break;
        // case ADJUST:
        //     oled_write_P(PSTR("Adjust\n"), false);
        //     break;
        default:
            oled_write_P(PSTR("Undefined\n"), false);
    }

    // // Host Keyboard LED Status
    // uint8_t led_usb_state = host_keyboard_leds();
    // oled_write_P(IS_LED_ON(led_usb_state, USB_LED_NUM_LOCK)    ? PSTR("NUMLCK ") : PSTR("       "), false);
    // oled_write_P(IS_LED_ON(led_usb_state, USB_LED_CAPS_LOCK)   ? PSTR("CAPLCK ") : PSTR("       "), false);
    // oled_write_P(IS_LED_ON(led_usb_state, USB_LED_SCROLL_LOCK) ? PSTR("SCRLCK ") : PSTR("       "), false);
}

bool oled_task_user(void) {
    if (is_keyboard_master()) {
      render_status(); // Renders the current keyboard state (layer, lock, caps, scroll, etc)
    } else {
      render_kyria_logo();
    }
    return false;
}
#endif



#ifdef LEADER_ENABLE
LEADER_EXTERNS();

void matrix_scan_user(void) {

  if (get_highest_layer(layer_state) == KL_MOUSE_AND_FCT) {
    if (last_input_activity_elapsed() > NUM_LAYER_TIMEOUT) {
      layer_off(KL_MOUSE_AND_FCT);
    }
  }

  
  LEADER_DICTIONARY() {
    leading = false;
    leader_end();

    // quote -> cut sound and video in Zoom
    SEQ_ONE_KEY(BP_QUOT) {
      register_code(KC_LALT);
      register_code(BP_A);
      unregister_code(BP_A);
      unregister_code(KC_LALT);
      register_code(KC_LALT);
      register_code(BP_V);
      unregister_code(BP_V);
      unregister_code(KC_LALT);
    }

    // Q -> save in emacs
    SEQ_ONE_KEY(BP_G) {
      register_code(KC_LCTL);
      register_code(BP_X);
      unregister_code(BP_X);
      register_code(BP_S);
      unregister_code(BP_S);
      unregister_code(KC_LCTL);
    }
    SEQ_ONE_KEY(BP_Q) { 
      SEND_STRING(SS_TAP(X_1) SS_TAP(X_1) SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_T) {
      SEND_STRING("()" SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_S) {
      SEND_STRING("{}" SS_TAP(X_LEFT));
    }

    SEQ_ONE_KEY(BP_R) {
      SEND_STRING("[]" SS_TAP(X_LEFT));
    }

    // E X
    SEQ_TWO_KEYS(BP_E, BP_X) {
      SEND_STRING("norman@xael.org");
    }
    // E H
    SEQ_TWO_KEYS(BP_E, BP_H) {
      SEND_STRING("alexandre.norman@hespul.org");
    }
  }
}
#endif

