# MCU name
MCU = atmega32u4

# Bootloader selection
#BOOTLOADER = atmel-dfu
BOOTLOADER = caterina

# Build Options
#   change yes to no to disable
#
BOOTMAGIC_ENABLE = yes      # Enable Bootmagic Lite
MOUSEKEY_ENABLE = yes       # Mouse keys
EXTRAKEY_ENABLE = yes       # Audio control and System control
CONSOLE_ENABLE = no         # Console for debug
COMMAND_ENABLE = no         # Commands for debug and configuration
# Do not enable SLEEP_LED_ENABLE. it uses the same timer as BACKLIGHT_ENABLE
SLEEP_LED_ENABLE = no       # Breathing sleep LED during USB suspend
# if this doesn't work, see here: https://github.com/tmk/tmk_keyboard/wiki/FAQ#nkro-doesnt-work
NKRO_ENABLE = no            # USB Nkey Rollover
BACKLIGHT_ENABLE = no       # Enable keyboard backlight functionality
RGBLIGHT_ENABLE = no        # Enable keyboard RGB underglow
AUDIO_ENABLE = no           # Audio output

FORCE_NKRO = no
COMBO_ENABLE = yes

# https://docs.qmk.fm/#/feature_pointing_device
POINTING_DEVICE_ENABLE = no
POINTING_DEVICE_DRIVER = pimoroni_trackball
PIMORONI_TRACKBALL_ENABLE = no
PIMORONI_TRACKBALL_ADDRESS = 0x0A
PIMORONI_TRACKBALL_TIMEOUT = 100
PIMORONI_TRACKBALL_INTERVAL_MS = 2
PIMORONI_TRACKBALL_SCALE = 5
PIMORONI_TRACKBALL_DEBOUNCE_CYCLES = 20
PIMORONI_TRACKBALL_ERROR_COUNT = 10

CONSOLE_ENABLE = yes

# TAP_DANCE_ENABLE = yes


# |`PIMORONI_TRACKBALL_ADDRESS`         | (Required) Sets the I2C Address for the Pimoroni Trackball.                        | `0x0A`  |
# |`PIMORONI_TRACKBALL_TIMEOUT`         | (Optional) The timeout for i2c communication with the trackpad in milliseconds.    | `100`   |
# |`PIMORONI_TRACKBALL_INTERVAL_MS`     | (Optional) The update/read interval for the sensor in milliseconds.                | `8`     |
# |`PIMORONI_TRACKBALL_SCALE`           | (Optional) The multiplier used to generate reports from the sensor.                | `5`     |
# |`PIMORONI_TRACKBALL_DEBOUNCE_CYCLES` | (Optional) The number of scan cycles used for debouncing on the ball press.        | `20`    |
# |`PIMORONI_TRACKBALL_ERROR_COUNT`     | (Optional) Specifies the number of read/write errors until the sensor is disabled. | `10`  |


ifeq ($(strip $(PIMORONI_TRACKBALL_ENABLE)), yes)
	# SRC += pimoroni_trackball.c
    OPT_DEFS += -DPIMORONI_TRACKBALL_ENABLE
    POINTING_DEVICE_ENABLE := yes
	PIMORONI_TRACKBALL_ADDRESS = 0x0A
    QUANTUM_LIB_SRC += i2c_master.c
endif

# OLED_ENABLE = yes
# OLED_DRIVER = SSD1306

