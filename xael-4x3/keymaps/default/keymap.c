#include QMK_KEYBOARD_H
#include "sendstring_bepo.h"
#include "quantum/keymap_extras/keymap_bepo.h"

// #ifdef PIMORONI_TRACKBALL_ENABLE
// #include "pimoroni_trackball.h"
// #endif

// #ifdef POINTING_DEVICE_ENABLE
// #include "pointing_device.h"
// #endif

//#include "print.h"

// #include "drivers/sensors/pimoroni_trackball.h"

enum combos {
  K78_0,
  K89_DOT,
  K12_RET,
};

const uint16_t PROGMEM k78_combo[] = {LSFT(KC_7), LSFT(KC_8), COMBO_END};
const uint16_t PROGMEM k89_combo[] = {LSFT(KC_8), LSFT(KC_9), COMBO_END};
const uint16_t PROGMEM k12_combo[] = {LSFT(KC_1), LSFT(KC_2), COMBO_END};


combo_t key_combos[COMBO_COUNT] = {
  [K78_0] = COMBO(k78_combo, LSFT(KC_0)),
  [K89_DOT] = COMBO(k89_combo, KC_V),
  [K12_RET] = COMBO(k12_combo, KC_ENT),
};

// ACTION_TAP_DANCE_DOUBLE(KC_SPC, KC_ENT)

// const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
//   [0] = LAYOUT(TO(1), RGUI(KC_1), RGUI(KC_2), RGUI(KC_3), RGUI(KC_4), RGUI(KC_5), RGUI(KC_6), RGUI(KC_7), RGUI(KC_0), LALT(KC_A), LALT(KC_U), RGUI(KC_9)),
//   [1] = LAYOUT(TO(2), KC_Q, KC_W, KC_E, KC_SPACE, KC_A, KC_S, KC_D, KC_ENTER, KC_Z, KC_X, KC_C),
//   [2] = LAYOUT(TO(3), KC_BTN1, KC_MS_U, KC_BTN2, KC_PGUP, KC_MS_L, KC_MS_D, KC_MS_R, KC_PGDOWN, KC_WH_U, KC_WBAK, KC_WH_D),
//   [3] = LAYOUT(TO(0), LSFT(KC_7), LSFT(KC_8), LSFT(KC_9), KC_SPACE, LSFT(KC_4), LSFT(KC_5), LSFT(KC_6), KC_SPACE, LSFT(KC_1), LSFT(KC_2), LSFT(KC_3))
// };
const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
  [0] = LAYOUT(
               KC_NO,         KC_NO,         LALT(BP_U),    LALT(BP_Y),
               KC_NO,         KC_NO,         LALT(BP_T),    LALT(BP_V),
               KC_NO,         KC_NO,         LALT(BP_S),    LALT(BP_A))
};


// void keyboard_post_init_user(void) {
//   // Customise these values to desired behaviour
//   // debug_enable=true;
//   // debug_matrix=true;
//   // debug_keyboard=true;
//   // debug_mouse=true;
// }

// #ifdef OLED_ENABLE
// static void render_logo(void) {
//     static const char PROGMEM qmk_logo[] = {
//         0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x88, 0x89, 0x8A, 0x8B, 0x8C, 0x8D, 0x8E, 0x8F, 0x90, 0x91, 0x92, 0x93, 0x94,
//         0xA0, 0xA1, 0xA2, 0xA3, 0xA4, 0xA5, 0xA6, 0xA7, 0xA8, 0xA9, 0xAA, 0xAB, 0xAC, 0xAD, 0xAE, 0xAF, 0xB0, 0xB1, 0xB2, 0xB3, 0xB4,
//         0xC0, 0xC1, 0xC2, 0xC3, 0xC4, 0xC5, 0xC6, 0xC7, 0xC8, 0xC9, 0xCA, 0xCB, 0xCC, 0xCD, 0xCE, 0xCF, 0xD0, 0xD1, 0xD2, 0xD3, 0xD4, 0x00
//     };
// 
//     oled_write_P(qmk_logo, false);
// }
// 
// bool oled_task_user(void) {
//     // Host Keyboard Layer Status
//     oled_write_P(PSTR("Layer: "), false);
// 
//     print("layer");
//     switch (get_highest_layer(layer_state)) {
//         case 0:
//             oled_write_P(PSTR("Default\n"), false);
//             break;
//         case 1:
//             oled_write_P(PSTR("FN\n"), false);
//             break;
//         case 2:
//             oled_write_P(PSTR("ADJ\n"), false);
//             break;
//         default:
//             // Or use the write_ln shortcut over adding '\n' to the end of your string
//             oled_write_ln_P(PSTR("Undefined"), false);
//             render_logo();
//     }
// 
//     // Host Keyboard LED Status
//     led_t led_state = host_keyboard_led_state();
//     oled_write_P(led_state.num_lock ? PSTR("NUM ") : PSTR("    "), false);
//     oled_write_P(led_state.caps_lock ? PSTR("CAP ") : PSTR("    "), false);
//     oled_write_P(led_state.scroll_lock ? PSTR("SCR ") : PSTR("    "), false);
//     return 0;
// }
// #endif


// #ifdef PIMORONI_TRACKBALL_ENABLE
// void run_trackball_cleanup(void) {
//     // if (trackball_is_scrolling()) {
//     //     trackball_set_rgbw(RGB_CYAN, 0x00);
//     // } else if (trackball_get_precision() != 1.0) {
//     //     trackball_set_rgbw(RGB_GREEN, 0x00);
//     // } else {
//     // trackball_set_rgbw(RGB_MAGENTA, 0x00);
//     // }
// }
// 
// void keyboard_post_init_keymap(void) {
//   // trackball_set_precision(1.5);
//     trackball_set_rgbw(RGB_MAGENTA, 0x00);
// }
// // void shutdown_keymap(void) { trackball_set_rgbw(RGB_RED, 0x00); }
// 
// 
// void trackball_register_button(bool pressed, enum mouse_buttons button) {
//     report_mouse_t currentReport = pointing_device_get_report();
//     if (pressed) {
//         currentReport.buttons |= button;
//     } else {
//         currentReport.buttons &= ~button;
//     }
//     pointing_device_set_report(currentReport);
// }
// #endif
// 
// #ifdef PIMORONI_TRACKBALL_ENABLE
// 
// void pointing_device_task() {
//     report_mouse_t mouse_report = pointing_device_get_report();
// 
//     // if (is_keyboard_master()) {
// 	// 	bool fast_scroll = (get_highest_layer(layer_state) == _SYM);
// 	// 	process_mouse(&mouse_report, fast_scroll);
//     // }
// 	// bool fast_scroll = false;
// 	//process_mouse(&mouse_report, fast_scroll);
//     // trackball_set_rgbw(130,120,170,150);
//     // trackball_set_rgbw(200,200,0,0);
//     //dprintf("%s string", get_highest_layer(layer_state));
// 
//     switch (get_highest_layer(layer_state)) {
//     case 0:
//       trackball_set_rgbw(150,0,0,0);
//       break;
//     case 1:
//       trackball_set_rgbw(130,0,170,0);
//       break;
//     case 2:
//       trackball_set_rgbw(0,0,150,0);
//       break;
//     case 3:
//       trackball_set_rgbw(0,170,130,0);
//       break;
//     default:
//       trackball_set_rgbw(0,0,0,150);
//     }
//     
//     // if (layer_state_is(_ADJUST)) {
//     //     trackball_set_scrolling(true);
//     // } else {
//     //     trackball_set_scrolling(false);
//     // }
// 
//     pointing_device_set_report(mouse_report);
//     pointing_device_send();
// }
// #endif
