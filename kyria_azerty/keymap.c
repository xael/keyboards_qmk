/* Copyright 2019 Thomas Baart <thomas@splitkb.com>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
#include QMK_KEYBOARD_H
#include "sendstring_french.h"
#include "quantum/keymap_extras/keymap_french.h"

uint16_t copy_paste_timer;


enum keymap_layer {
  KL_BASE = 0,
  KL_MACRO,
  KL_FCT,
  KL_CURSOR_AND_NUMBERS,
  KL_MOUSE,
  KL_OSL,
  KL_EMPTY,
};



// https://docs.qmk.fm/#/feature_tap_dance?id=example-4-39quad-function-tap-dance39
typedef enum {
  TD_NONE,
  TD_UNKNOWN,
  TD_SINGLE_TAP,
  TD_SINGLE_HOLD,
  TD_DOUBLE_TAP,
  TD_DOUBLE_HOLD,
  TD_DOUBLE_SINGLE_TAP, // Send two single taps
  TD_TRIPLE_TAP,
  TD_TRIPLE_HOLD
} td_state_t;

typedef struct {
  bool is_press_action;
  td_state_t state;
} td_tap_t;


// Tap Dance declarations
enum {
  TD_WBAK_WFWD,
  TD_C_CCEDIL,
  TD_E_EACUTE,
  TD_A_AGRAVE,
  TD_AROBASE_UNDERSCORE,
  // TD_ESC_ESCX,
  // TD_LT2_SPACE_UNDERSCORE,
  // TD_TAB_SHIFT,
};


void post_process_record_user(uint16_t keycode, keyrecord_t *record) {
  switch (keycode) {
  case PB_32:
    if (record->event.pressed) {
      tap_code(KC_NUMLOCK);
      wait_ms(20);
      tap_code(KC_NUMLOCK);
    }
    break;
  case PB_31:
    if (record->event.pressed) {
      tap_code(KC_CAPSLOCK);
      wait_ms(250);
      tap_code(KC_CAPSLOCK);
    }
    break;
  case PB_30:
    if (record->event.pressed) {
      tap_code(KC_CAPSLOCK);
      wait_ms(20);
      tap_code(KC_NUMLOCK);
      wait_ms(250);
      tap_code(KC_NUMLOCK);
      wait_ms(20);
      tap_code(KC_CAPSLOCK);
    }
    break;
 }
}


td_state_t cur_dance(qk_tap_dance_state_t *state) {
  if (state->count == 1) {
    if (state->interrupted || !state->pressed) return TD_SINGLE_TAP;
    // Key has not been interrupted, but the key is still held. Means you want to send a 'HOLD'.
    else return TD_SINGLE_HOLD;
  } else if (state->count == 2) {
    // TD_DOUBLE_SINGLE_TAP is to distinguish between typing "pepper", and actually wanting a double tap
    // action when hitting 'pp'. Suggested use case for this return value is when you want to send two
    // keystrokes of the key, and not the 'double tap' action/macro.
    if (state->interrupted) return TD_DOUBLE_SINGLE_TAP;
    else if (state->pressed) return TD_DOUBLE_HOLD;
    else return TD_DOUBLE_TAP;
  }

  // Assumes no one is trying to type the same letter three times (at least not quickly).
  // If your tap dance key is 'KC_W', and you want to type "www." quickly - then you will need to add
  // an exception here to return a 'TD_TRIPLE_SINGLE_TAP', and define that enum just like 'TD_DOUBLE_SINGLE_TAP'
  if (state->count == 3) {
    if (state->interrupted || !state->pressed) return TD_TRIPLE_TAP;
    else return TD_TRIPLE_HOLD;
  } else return TD_UNKNOWN;
}



// Create an instance of 'td_tap_t' for the 'esc' tap dance.
static td_tap_t ctap_state = {
  .is_press_action = true,
  .state = TD_NONE
};

void c_ccedil_finished(qk_tap_dance_state_t *state, void *user_data) {
  ctap_state.state = cur_dance(state);
  switch (ctap_state.state) {
  case TD_SINGLE_TAP: register_code(FR_C); break;
  case TD_SINGLE_HOLD: register_code(KC_LSFT); register_code(FR_C); break;
  case TD_DOUBLE_TAP: register_code(FR_CCED); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}

void c_ccedil_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (ctap_state.state) {
  case TD_SINGLE_TAP: unregister_code(FR_C); break;
  case TD_SINGLE_HOLD: unregister_code(FR_C); unregister_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: unregister_code(FR_CCED); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  ctap_state.state = TD_NONE;
}

// Create an instance of 'td_tap_t' for the 'esc' tap dance.
static td_tap_t etap_state = {
  .is_press_action = true,
  .state = TD_NONE
};
 
void e_eacute_finished(qk_tap_dance_state_t *state, void *user_data) {
  etap_state.state = cur_dance(state);
  switch (etap_state.state) {
  case TD_SINGLE_TAP: register_code(FR_E); break;
  case TD_SINGLE_HOLD: register_code(KC_LSFT); register_code(FR_E); break;
  case TD_DOUBLE_TAP: register_code(FR_EACU); break;
  case TD_DOUBLE_HOLD: register_code(FR_EGRV); break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}

void e_eacute_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (etap_state.state) {
  case TD_SINGLE_TAP: unregister_code(FR_E); break;
  case TD_SINGLE_HOLD: unregister_code(FR_E); unregister_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: unregister_code(FR_EACU); break;
  case TD_DOUBLE_HOLD: unregister_code(FR_EGRV); break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  etap_state.state = TD_NONE;
}

// Create an instance of 'td_tap_t' for the 'esc' tap dance.
static td_tap_t atap_state = {
  .is_press_action = true,
  .state = TD_NONE
};

void a_agrave_finished(qk_tap_dance_state_t *state, void *user_data) {
  atap_state.state = cur_dance(state);
  switch (atap_state.state) {
  case TD_SINGLE_TAP: register_code(FR_A); break;
  case TD_SINGLE_HOLD: register_code(KC_LSFT); register_code(FR_A); break;
  case TD_DOUBLE_TAP: register_code(FR_AGRV); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
}

void a_agrave_reset(qk_tap_dance_state_t *state, void *user_data) {
  switch (atap_state.state) {
  case TD_SINGLE_TAP: unregister_code(FR_A); break;
  case TD_SINGLE_HOLD: unregister_code(FR_A); unregister_code(KC_LSFT); break;
  case TD_DOUBLE_TAP: unregister_code(FR_AGRV); break;
  case TD_DOUBLE_HOLD: break;
  case TD_DOUBLE_SINGLE_TAP: break;
  case TD_NONE: break;
  case TD_UNKNOWN: break; 
  case TD_TRIPLE_TAP: break;
  case TD_TRIPLE_HOLD: break;
  }
  atap_state.state = TD_NONE;
}


// Tap Dance definitions
qk_tap_dance_action_t tap_dance_actions[] = {
  [TD_WBAK_WFWD] = ACTION_TAP_DANCE_DOUBLE(KC_WBAK, KC_WFWD),
  [TD_C_CCEDIL] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, c_ccedil_finished, c_ccedil_reset),
  [TD_E_EACUTE] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, e_eacute_finished, e_eacute_reset),
  [TD_A_AGRAVE] = ACTION_TAP_DANCE_FN_ADVANCED(NULL, a_agrave_finished, a_agrave_reset),
  [TD_AROBASE_UNDERSCORE] = ACTION_TAP_DANCE_DOUBLE(KC_1, KC_8),
};



const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {
/*
 * Base Layer: AZERTY...
 *
 * ,-------------------------------------------.                              ,-------------------------------------------.
 * |    &_  |   A  |   Z  |   E  |   R  |   T  |                              |   Y  |   U  |   I  |   O  |   P  |   ^¨   |
 * |--------+------+------+------+------+------|                              |------+------+------+------+------+--------|
 * |    $   |   Q  |   S  |  D   |   F  |   G  |                              |   H  |   J  |   K  |   L  |   M  |   ù%   |
 * |--------+------+------+------+------+------+-------------.  ,-------------+------+------+------+------+------+--------|
 * |    <   |   W  |   X  |   C  |   V  |   B  | ESC  | F23  |  | COPY | OSL  |   N  |   ,? |   ;. |  /:  |  !§  |   *µ   |
 * `----------------------+------+------+------+------+------|  |------+------+------+------+------+----------------------'
 *                        | Lalt | WIN  | RET  | Lctl | F24  |  | PASTE| TAB  | SPC  |  TG  | TG   |
 *                        |      |      | Ralt | Spce |      |  |      | shift| curs | mouse| numb |
 *                        `----------------------------------'  `----------------------------------'
 */

// Manque & 
   [KL_BASE] = LAYOUT(
      TD(TD_AROBASE_UNDERSCORE),    TD(TD_A_AGRAVE),      FR_Z,                   TD(TD_E_EACUTE),        FR_R,      FR_T,                                                              FR_Y,    FR_U,     FR_I,    FR_O,     FR_P,     FR_CIRC,
      FR_DLR,     FR_Q,                 FR_S,                   FR_D,                   FR_F,      FR_G,                                                              FR_H,    FR_J,     FR_K,    FR_L,     FR_M,     FR_UGRV,
      FR_LABK,   FR_W,                 FR_X,                   TD(TD_C_CCEDIL),        FR_V,      FR_B,     KC_ESC,  KC_RALT,     C(KC_INS), MO(KL_MACRO),    FR_N,    FR_COMM,  FR_SCLN, FR_COLN,  FR_EXLM,  FR_ASTR,
      
      KC_LGUI,   KC_LALT,   KC_LCTL,   KC_ENT,   KC_LSFT,              S(KC_INS), KC_TAB,  LT(KL_CURSOR_AND_NUMBERS, KC_SPACE), TG(KL_MOUSE),   OSL(KL_FCT)
    ),
 
  [KL_MACRO] = LAYOUT(  // Macros
               KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,                                                                  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  
               PB_31,  KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,                                                                  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  
               PB_32,  KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,        KC_NO,   KC_NO,                 DM_REC1,   KC_TRNS,       KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  
                       KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,                                               DM_PLY1,  DM_RSTP,  KC_NO,  KC_NO,  KC_NO
                 ),
  
  [KL_FCT ] = LAYOUT(  // Function keys   
               KC_NO,  KC_F1,     KC_F2,   KC_F3,    KC_F4,    KC_F5,                                                    FR_AT,    FR_EURO,  FR_GRV,   FR_DEG,   FR_PERC,  FR_DIAE,
               KC_NO,  KC_F6,     KC_F7,   KC_F8,    KC_F9,    KC_F10,                                                   FR_QUOT,  FR_LPRN,  FR_LBRC,  FR_LCBR,  FR_PIPE,  FR_TILD,  
               KC_NO,  KC_NO,     KC_NO,   KC_NO,    KC_NO,    KC_NO,   KC_NO,  KC_NO,   KC_NO,  KC_NO,                  FR_DQUO,  FR_RPRN,  FR_RBRC,  FR_RCBR,  FR_HASH,  FR_BSLS,  
               KC_NO,  KC_NO,     KC_NO,   KC_NO,    KC_NO,                                                              KC_NO,    KC_NO,     KC_NO,   KC_NO,    KC_NO
               ),

  [KL_CURSOR_AND_NUMBERS] = LAYOUT(  // Cursor & numbers                                                      
               KC_NO,    FR_1,      FR_2,    FR_3,     FR_4,     FR_5,                                                      KC_INS,   KC_HOME,   KC_UP,   KC_END,   C(KC_PGUP),  C(FR_W),
               KC_NO,    FR_6,      FR_7,    FR_8,     FR_9,     FR_0,                                                      KC_BSPC,  KC_LEFT,   KC_DOWN, KC_RGHT,  C(KC_PGDN),  FR_MICR,
               FR_PERC,  FR_PLUS,   FR_MINS, FR_SLSH,  FR_ASTR,  FR_EQL,    KC_NO,    KC_NO,     DM_REC1,    DM_REC2,           KC_DEL,   KC_PGUP,   KC_NO,   KC_PGDN,  KC_NO,    A(FR_PERC),
               KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,                                                                   DM_PLY1,   DM_PLY2,   KC_NO,   KC_NO,   KC_NO
               ),                                                                                                                           
  [KL_MOUSE] = LAYOUT(  // Mouse & functions                                                        
               KC_NO,  KC_NO,     KC_NO,   KC_NO,    KC_NO,    KC_NO,                                                      KC_NO,           KC_BTN1,   KC_MS_U, KC_BTN2,  KC_WHOM,  C(FR_W),
               PB_31,  KC_NO,     KC_NO,   KC_NO,    KC_NO,    KC_NO,                                                      KC_MS_WH_LEFT,   KC_MS_L,   KC_MS_D, KC_MS_R,  KC_WH_D,  C(KC_PGUP),
               PB_32,  KC_NO,     KC_NO,   KC_NO,    KC_NO,    KC_NO,            KC_NO,  KC_NO,   KC_NO,  KC_WH_U,         KC_MS_WH_RIGHT,  KC_BTN3,   KC_NO,   KC_BTN4,  TD(TD_WBAK_WFWD),    C(KC_PGDN),
               KC_BTN1,  KC_BTN2,  KC_LCTL, KC_BTN3,  KC_LSFT,        KC_NO, KC_WH_D, KC_NO, TG(KL_MOUSE), KC_NO
                 ),
  [KL_OSL] = LAYOUT(  // OSL                                                      
               KC_MAIL,   KC_1,      KC_2,    KC_3,     KC_4,     KC_5,                                                       KC_6,     KC_7,      KC_8,    KC_9,     KC_0,     KC_EQL,
               KC_F21,    KC_NO,     KC_NO,   KC_NO,    KC_NO,    KC_NO,                                                      KC_1,     KC_4,      ALGR(KC_X),   ALGR(KC_4),    ALGR(KC_2),  KC_NO,
               KC_F22,    KC_F24,    KC_F23,  KC_NO,    KC_NO,    KC_NO,     KC_NO,    KC_NO,     KC_NO,    KC_NO,            KC_1,     KC_5,      ALGR(KC_C),   ALGR(KC_5),    ALGR(KC_3),  S(KC_EQL),
               G(KC_1),  G(KC_2),  G(KC_3),  G(KC_4),  G(KC_5),    G(KC_6),  G(KC_7),  G(KC_8),  G(KC_9),  G(KC_0)
                 ),
    
  [KL_EMPTY] = LAYOUT(  // EMPTY coz bug with OSL which does not work without
               KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,                                               KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  
               KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,                                               KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  
               KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,        KC_NO,   KC_NO,  KC_NO,   KC_NO,       KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO,  
                       KC_NO,  KC_NO,  KC_NO,  KC_NO,   KC_NO,                                               KC_NO,  KC_NO,  KC_NO,  KC_NO,  KC_NO
                 )
    
};




#ifdef OLED_ENABLE
oled_rotation_t oled_init_user(oled_rotation_t rotation) {
  return OLED_ROTATION_180;
}

static void render_kyria_logo(void) {
  static const char PROGMEM kyria_logo[] = {
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf0, 0xf0, 0xc0, 0x80, 
	0x80, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x80, 0x80, 0x80, 0xc0, 0xf0, 0xf0, 0xf0, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x07, 0x0f, 0xf9, 0xf9, 
	0x07, 0x07, 0x07, 0x06, 0x06, 0x1e, 0x1c, 0xf8, 0xf8, 0xf8, 0xf8, 0xf0, 0xe0, 0xe0, 0xe0, 0xe0, 
	0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xe0, 0xf0, 0xf8, 0xf8, 0xf8, 0xf8, 0xfc, 0x1e, 0x1e, 0x06, 
	0x07, 0x07, 0x07, 0xf9, 0xf9, 0xf9, 0x0f, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x80, 0x80, 0x80, 0xc0, 0xc0, 0xc0, 0xc0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0f, 0x1f, 
	0xfc, 0xfc, 0xfc, 0xff, 0xff, 0x3f, 0x3f, 0x0f, 0x0f, 0xcf, 0xcf, 0x0f, 0x0f, 0x0f, 0x3f, 0x3f, 
	0xff, 0xff, 0xff, 0xff, 0x3f, 0x3f, 0x0f, 0x0f, 0xcf, 0xcf, 0x0f, 0x0f, 0x0f, 0x3f, 0x3f, 0xff, 
	0xff, 0xfc, 0xfc, 0x0f, 0x0f, 0x07, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xf8, 0xfc, 0xff, 0xff, 
	0xff, 0x9f, 0x8f, 0x03, 0x03, 0x0f, 0x1f, 0xff, 0xfe, 0xfc, 0xf8, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x7f, 0xff, 0xff, 0xff, 0xff, 0xe0, 0xe0, 0x80, 0x80, 0x8f, 0x8f, 0x80, 0x80, 0x80, 0xe0, 0x60, 
	0x7f, 0x7f, 0x7f, 0x7f, 0xe0, 0xe0, 0x80, 0x80, 0x8f, 0x8f, 0x80, 0x80, 0x80, 0xe0, 0xe0, 0xff, 
	0xff, 0xff, 0x7f, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x01, 0x03, 0x07, 
	0x07, 0x07, 0x87, 0xc0, 0xe0, 0xfe, 0xff, 0x7f, 0x1f, 0x0f, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x03, 0x87, 0xc7, 0xc7, 0xef, 0xff, 0xff, 0x3f, 0x7f, 0xff, 0xff, 0xff, 0xff, 0xfe, 
	0xfe, 0xfe, 0xfe, 0xfe, 0xff, 0xff, 0xff, 0xff, 0x7f, 0x3f, 0x1f, 0x1f, 0x1f, 0x0f, 0x07, 0x07, 
	0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0xfe, 0xff, 0xff, 0xff, 0x0f, 0x03, 0x00, 0x00, 0x00, 0x00, 0x00, 0x80, 0xe0, 0xf0, 0xf8, 
	0xfc, 0xfe, 0xff, 0xff, 0xff, 0xff, 0xff, 0xf9, 0xe0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x01, 0x03, 0x1f, 0x3f, 0x7f, 0xfe, 0xfc, 0xf8, 0xf0, 0xf0, 0xf8, 0xff, 0xff, 0xff, 0xff, 
	0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xfe, 0xfc, 0xf8, 0xf0, 0xe0, 0xe0, 
	0xc0, 0x80, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01, 0x03, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 
	0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 0x0f, 
	0x0f, 0x0f, 0x0f, 0x0e, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 
	0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00

  };
  oled_write_raw_P(kyria_logo, sizeof(kyria_logo));
}

// static void render_qmk_logo(void) {
//   static const char PROGMEM qmk_logo[] = {
//     0x80,0x81,0x82,0x83,0x84,0x85,0x86,0x87,0x88,0x89,0x8a,0x8b,0x8c,0x8d,0x8e,0x8f,0x90,0x91,0x92,0x93,0x94,
//     0xa0,0xa1,0xa2,0xa3,0xa4,0xa5,0xa6,0xa7,0xa8,0xa9,0xaa,0xab,0xac,0xad,0xae,0xaf,0xb0,0xb1,0xb2,0xb3,0xb4,
//     0xc0,0xc1,0xc2,0xc3,0xc4,0xc5,0xc6,0xc7,0xc8,0xc9,0xca,0xcb,0xcc,0xcd,0xce,0xcf,0xd0,0xd1,0xd2,0xd3,0xd4,0};
// 
//   oled_write_P(qmk_logo, false);
// }

static void render_status(void) {
  // QMK Logo and version information
  // render_qmk_logo();
  oled_write_P(PSTR("Sabine\n\n"), false);

  // Host Keyboard Layer Status
  oled_write_P(PSTR("Couche: "), false);
  switch (get_highest_layer(layer_state)) {
  case KL_BASE:
    oled_write_P(PSTR("AZERTY\n        Rose\n"), false);
    break;
  case KL_FCT:
    oled_write_P(PSTR("F1-F10 / @#\n        Vert\n"), false);
    break;
  case KL_CURSOR_AND_NUMBERS:
    oled_write_P(PSTR("Curseur / Nb\n        Bleu\n"), false);
    break;
  case KL_MOUSE:
    oled_write_P(PSTR("Souris\n        Bleu\n"), false);
    break;
  case KL_OSL:
    oled_write_P(PSTR("OSL\n"), false);
    break;
  case KL_MACRO:
    oled_write_P(PSTR("Macro\n"), false);
    break;
  default:
    oled_write_P(PSTR("Undefined\n"), false);
  }

  #ifdef WPM_ENABLE
  oled_write_P(PSTR("\nWPM:    "), false);
  oled_write(get_u8_str(get_current_wpm(), '0'), false);
  oled_write_P(PSTR("\n"), false);
  #endif

  // // Host Keyboard LED Status
  // uint8_t led_usb_state = host_keyboard_leds();
  // oled_write_P(IS_LED_ON(led_usb_state, USB_LED_NUM_LOCK)    ? PSTR("NUMLCK ") : PSTR("       "), false);
  // oled_write_P(IS_LED_ON(led_usb_state, USB_LED_CAPS_LOCK)   ? PSTR("CAPLCK ") : PSTR("       "), false);
  // oled_write_P(IS_LED_ON(led_usb_state, USB_LED_SCROLL_LOCK) ? PSTR("SCRLCK ") : PSTR("       "), false);
}

bool oled_task_user(void) {
  if (is_keyboard_master()) {
    render_status(); // Renders the current keyboard state (layer, lock, caps, scroll, etc)
  } else {
    render_kyria_logo();
  }
  return false;
}
#endif


#ifdef DYNAMIC_MACRO_ENABLE
// void dynamic_macro_record_start_user(void) {
//   oled_write_P(PSTR("RECORDING\n"), false);
// }
// void dynamic_macro_record_key_user(int8_t direction, keyrecord_t *record) {
//   oled_write_P(PSTR("RECORDING\n"), false);
// }
// void dynamic_macro_record_end_user(int8_t direction) {
//   oled_write_P(PSTR("StOp RDING\n"), false);
// }
#endif


