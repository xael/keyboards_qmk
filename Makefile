4x3:
	cd qmk_firmware && make handwired/xael4x3:default
	cd qmk_firmware && make handwired/xael4x3:default:flash

dactyl:
	cd qmk_firmware && make MCU=atmega32u4 BOOTLOADER=qmk-dfu handwired/dactyl_manuform/4x6:alexandrenorman:dfu-split-left

dactyl_right:
	cd qmk_firmware && make MCU=atmega32u4 BOOTLOADER=caterina handwired/dactyl_manuform/4x6:alexandrenorman:avrdude-split-right

macropad:
	cd qmk_firmware && make MCU=atmega32u4 BOOTLOADER=caterina handwired/xael4x3:default:avrdude

blackpill:
	# cd qmk_firmware && make MCU=STM32F401 BOOTLOADER=stm32-dfu handwired/dactyl_manuform/4x6:alexandrenorman && dfu-util -a 0 -s 0x8000000 -RD handwired_dactyl_manuform_4x6_alexandrenorman.bin
	cd qmk_firmware && make BOOTLOADER=stm32-dfu handwired/dactyl_manuform/4x6:alexandrenorman && dfu-util -a 0 -s 0x8000000 -RD handwired_dactyl_manuform_4x6_alexandrenorman.bin

setup:
	git submodule init
	git submodule update
	cd qmk_firmware && make git-submodule
	test -s qmk_firmware/keyboards/handwired/dactyl_manuform/4x6/keymaps/alexandrenorman || ln -s ../../../../../../dactyl_bepo qmk_firmware/keyboards/handwired/dactyl_manuform/4x6/keymaps/alexandrenorman
	test -s qmk_firmware/keyboards/handwired/xael-4x3 || ln -s ../../../xael-4x3 qmk_firmware/keyboards/handwired/xael4x3
	test -s qmk_firmware/keyboards/splitkb/kyria/keymaps/bepo || ln -s ../../../../../kyria_bepo qmk_firmware/keyboards/splitkb/kyria/keymaps/bepo
	test -s qmk_firmware/keyboards/splitkb/kyria/keymaps/azerty || ln -s ../../../../../kyria_azerty qmk_firmware/keyboards/splitkb/kyria/keymaps/azerty
	cd ../ploopy/qmk-firmware-ploopyco-dev && make git-submodule
	rm keyboards/ploopyco/trackball_nano/config.h
	ln -s ../../../../../trackball_nano__config.h keyboards/ploopyco/trackball_nano/config.h


kyria:
	cd qmk_firmware && make splitkb/kyria/rev2:bepo
	cd qmk_firmware && make splitkb/kyria/rev2:bepo:flash

kyria_azerty:
	cd qmk_firmware && make splitkb/kyria/rev2:azerty
	cd qmk_firmware && make splitkb/kyria/rev2:azerty:flash


ploopy_nano:
	cd ploopy/qmk-firmware-ploopyco-dev && qmk compile -kb ploopyco/trackball_nano/rev1_001 -km default
	cd ploopy/qmk-firmware-ploopyco-dev && qmk flash -kb ploopyco/trackball_nano/rev1_001 -km default

ploopy_nano_lkbm:
	cd ploopy/qmk-firmware-ploopyco-dev && qmk compile -kb ploopyco/trackball_nano/rev1_001 -km lkbm
	cd ploopy/qmk-firmware-ploopyco-dev && qmk flash -kb ploopyco/trackball_nano/rev1_001 -km lkbm

.PHONY: left right setup kyria_azerty ploopy_nano
